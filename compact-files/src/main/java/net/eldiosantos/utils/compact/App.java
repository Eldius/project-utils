package net.eldiosantos.utils.compact;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.eldiosantos.utils.compact.zip.ZipFileCompactor;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws IOException {
		// compactFiles();
		new ZipFileCompactor().compactFolder("C:\\dev\\testes\\ansible", "target.zip");
		new ZipFileCompactor().compactFiles(Arrays.asList(new File("applicationLogger.log")), "file.zip");
		System.out.println("Done");
	}

	public static void compactFiles() {
		byte[] buffer = new byte[1024];
		final String rootPath = "C:\\dev\\testes\\testes-activity-bpm";

		try {

			FileOutputStream fos = new FileOutputStream("target.zip");
			ZipOutputStream zos = new ZipOutputStream(fos);
			ZipEntry ze = new ZipEntry("applicationLogger.log");
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(rootPath
					+ "\\applicationLogger.log");

			int len;
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}

			in.close();
			zos.closeEntry();

			// remember close it
			zos.close();

			System.out.println("Done");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
