package net.eldiosantos.utils.compact.zip;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface FileCompactor {

	public abstract void compactFolder(String folder, String outputFile)
			throws IOException;

	public abstract void compactFiles(List<File> files, String outputFile)
			throws IOException;

}