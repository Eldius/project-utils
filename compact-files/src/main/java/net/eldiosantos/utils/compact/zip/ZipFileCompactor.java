package net.eldiosantos.utils.compact.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZipFileCompactor implements FileCompactor {

	private static final Logger logger = LoggerFactory.getLogger(ZipFileCompactor.class);

	/* (non-Javadoc)
	 * @see net.eldiosantos.utils.compact.FileCompactor#compactFolder(java.lang.String, java.lang.String)
	 */
	@Override
	public void compactFolder(final String folder, final String outputFile) throws IOException {
   		FileOutputStream fos = new FileOutputStream(outputFile);
		ZipOutputStream zos = new ZipOutputStream(fos);

		File root = new File(folder);

		List<File>files = getFilesFromFolder(root);

		logger.debug("Files returned: " + files.size());

		for(File f: files) {
			addFileToZip(zos, root, f);
		}
		zos.close();
	}

	/* (non-Javadoc)
	 * @see net.eldiosantos.utils.compact.FileCompactor#compactFiles(java.util.List, java.lang.String)
	 */
	@Override
	public void compactFiles(final List<File> files, final String outputFile) throws IOException {
   		FileOutputStream fos = new FileOutputStream(outputFile);
		ZipOutputStream zos = new ZipOutputStream(fos);

		logger.debug("Files returned: " + files.size());

		for(File f: files) {
			addFileToZip(zos, f);
		}
		zos.close();
	}


	public void addFileToZip(ZipOutputStream zos, File f)
			throws IOException {
		zos.putNextEntry(createZipEntry(f, null));
		writeFile(f, zos);
		zos.closeEntry();
	}

	public void addFileToZip(ZipOutputStream zos, File root, File f)
			throws IOException {
		zos.putNextEntry(createZipEntry(f, root));
		writeFile(f, zos);
		zos.closeEntry();
	}

	private void writeFile(final File file, final ZipOutputStream zos) throws IOException {
		final byte[] buffer = new byte[1024];

		final FileInputStream in = new FileInputStream(file);

		int len;
		while ((len = in.read(buffer)) > 0) {
			zos.write(buffer, 0, len);
		}
		in.close();
	}

	private ZipEntry createZipEntry(final File file, final File root) {
		String fileName = null;
		if(root != null) {
			fileName = file.getAbsolutePath().replace(root.getAbsolutePath(), "");
		} else {
			fileName = file.getName();
		}
		logger.debug("Creating zip entry for: " + fileName);
		ZipEntry ze = new ZipEntry(fileName.substring(1));

		return ze;
	}

	private List<File>getFilesFromFolder(final File folder) {
		logger.debug("Looking for files in " + folder.getAbsolutePath());
		List<File>files = new ArrayList<>();

		final File[] listFiles = folder.listFiles();

		for(File f:listFiles) {
			if(f.isDirectory()) {
				files.addAll(getFilesFromFolder(f));
			} else {
				files.add(f);
			}
		}

		return files;
	}
}
