#!/bin/bash

apt-get install -y tomcat7

cp /config-scripts/config-files/mysql/jdbc/main/mysql-connector-java-5.1.27.jar /usr/share/tomcat7/lib/
cp config-scripts/config-files/tomcat7 /etc/default/

service tomcat7 restart

