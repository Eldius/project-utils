package net.eldiosantos.logging;
import java.io.IOException;
import java.util.List;

import net.eldiosantos.logging.appender.LogEntry;
import net.eldiosantos.logging.appender.appender.DatabaseOperator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	private final static Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws IOException, InterruptedException {
		logger.debug("Holy crapk!", new IllegalArgumentException("Ooops."));
		logger.info("Finalizing process...");

		Thread.sleep(2000);

		final List<LogEntry> log = DatabaseOperator.getInstance().showLog();
		
		System.out.println("TAMANHO: " + log.size());
		for(LogEntry l:log) {
			System.out.println("message:\n" + l.getMessage());
			System.out.println("throwable:\n" + l.getThrowable());
		}
	}
}
