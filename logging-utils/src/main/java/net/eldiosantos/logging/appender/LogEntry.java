package net.eldiosantos.logging.appender;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class LogEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1729581478065051562L;

	private Long id;
	private String category;
	private String thread;
	private Date logdate;
	private String location;
	private String message;
	private String throwable;

	public LogEntry(Long id, String category, String thread, Date logdate,
			String location, String message, String throwable) {
		super();
		this.id = id;
		this.category = category;
		this.thread = thread;
		this.logdate = logdate;
		this.location = location;
		this.message = message;
		this.throwable = throwable;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LogEntry() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getThread() {
		return thread;
	}

	public void setThread(String thread) {
		this.thread = thread;
	}

	public Date getLogdate() {
		return logdate;
	}
	public java.sql.Date getSqlLogdate() {
		return new java.sql.Date(getLogdate().getTime());
	}

	public void setLogdate(Date logdate) {
		this.logdate = logdate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getThrowable() {
		return throwable;
	}

	public void setThrowable(String throwable) {
		this.throwable = throwable;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
