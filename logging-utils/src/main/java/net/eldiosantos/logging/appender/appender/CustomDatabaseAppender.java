package net.eldiosantos.logging.appender.appender;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

public class CustomDatabaseAppender extends AppenderSkeleton {

	static {
		Logger.getLogger("net.eldiosantos.logging.appender.appender.database.mybatis.LogEntryMapper").setLevel(Level.OFF);
//		Logger.getLogger("org.apache.ibatis").setLevel(Level.OFF);
	}

	private String url;
	private String user;
	private String pass;
	private String driver;
	private String dataSourceName;
	private String createTable;

	private final DatabaseOperator database = DatabaseOperator.getInstance();

	public void close() {
		System.out.println("Closing appender...");
		database.close();
	}

	public boolean requiresLayout() {
		return false;
	}

	@Override
	protected void append(LoggingEvent event) {
		database.insert(event);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getCreateTable() {
		return createTable;
	}

	public void setCreateTable(String createTable) {
		this.createTable = createTable!=null?createTable:"false";
	}
}
