package net.eldiosantos.logging.appender.appender;

import java.util.List;

import net.eldiosantos.logging.appender.LogEntry;
import net.eldiosantos.logging.appender.appender.database.LogEntryFactory;
import net.eldiosantos.logging.appender.appender.database.LogTableCreator;
import net.eldiosantos.logging.appender.appender.database.LogTableValidator;
import net.eldiosantos.logging.appender.appender.database.mybatis.LogEntryMapper;
import net.eldiosantos.logging.appender.appender.database.mybatis.SqlSessionFactoryCreator;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.spi.LoggingEvent;

public class DatabaseOperator {

	private final SqlSession session;
	private final LogEntryMapper logEntryMapper;
	private final LogEntryFactory logEntryFactory;

	private final LogTableCreator logTableCreator;

	private static DatabaseOperator instance = null;

	private DatabaseOperator() {
		session = new SqlSessionFactoryCreator().getFactory().openSession();
		logEntryMapper = session.getMapper(LogEntryMapper.class);
		logEntryFactory = new LogEntryFactory();
		logTableCreator = new LogTableCreator(session.getConnection());
		createTable();
	}

	public void insert(LoggingEvent log) {
		try {
			final LogEntry entry = logEntryFactory.create(log);
			logEntryMapper.save(entry);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createTable() {
		try {
			System.out.println("Creating table...");
			if(!new LogTableValidator(logEntryMapper).validate()) {
				logTableCreator.createTable();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<LogEntry>showLog() {
		return logEntryMapper.list();
	}

	public static DatabaseOperator getInstance() {
		if(instance == null) {
			instance = new DatabaseOperator();
		}
		return instance;
	}

	public void close() {
		session.close();
		instance = null;
	}
}
