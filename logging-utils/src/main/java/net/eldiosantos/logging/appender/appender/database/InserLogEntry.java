package net.eldiosantos.logging.appender.appender.database;

import net.eldiosantos.logging.appender.LogEntry;
import net.eldiosantos.logging.appender.appender.database.mybatis.LogEntryMapper;

public class InserLogEntry {

	private LogEntryMapper logEntryMapper;

	public InserLogEntry(LogEntryMapper logEntryMapper) {
		super();
		this.logEntryMapper = logEntryMapper;
	}

	public void insert(LogEntry entry) {
		logEntryMapper.save(entry);
	}
}
