package net.eldiosantos.logging.appender.appender.database;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.spi.LoggingEvent;

import net.eldiosantos.logging.appender.LogEntry;

public class LogEntryFactory {

	private static AtomicInteger count = new AtomicInteger(0);

	public LogEntry create(LoggingEvent event) {
		LogEntry log = new LogEntry();
		log.setCategory(event.getLoggerName());
		log.setLocation(event.getLocationInformation().fullInfo);
		log.setLogdate(new Date(event.getTimeStamp()));
		log.setMessage(event.getMessage().toString());
		log.setThread(event.getThreadName());

		if(event.getThrowableInformation() != null){
			StringBuffer throwable = new StringBuffer();
			for(String s:event.getThrowableInformation().getThrowableStrRep()) {
				throwable.append(s).append("\n");
			}
			log.setThrowable(throwable.toString());
		}

		return log;
	}
}
