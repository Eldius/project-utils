package net.eldiosantos.logging.appender.appender.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class LogTableCreator {

	private Connection connection;

	public LogTableCreator(Connection connection) {
		super();
		this.connection = connection;
	}

	public void createTable() {
		StringBuffer sql = new StringBuffer();

		Scanner sc = new Scanner(this.getClass().getClassLoader().getResourceAsStream("log_entry.sql"));
		while(sc.hasNext()) {
			sql.append(sc.nextLine()).append("\n");
		}
		Statement st;
		try {
			st = connection.createStatement();
			st.addBatch(sql.toString());
			int[] results = st.executeBatch();
			for(int i:results) {
				System.out.println("result: " + i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sc.close();
		}
	}
}
