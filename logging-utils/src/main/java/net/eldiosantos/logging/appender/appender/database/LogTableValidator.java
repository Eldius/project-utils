package net.eldiosantos.logging.appender.appender.database;

import net.eldiosantos.logging.appender.appender.database.mybatis.LogEntryMapper;

public class LogTableValidator {

	private LogEntryMapper logEntryMapper;

	public LogTableValidator(LogEntryMapper logEntryMapper) {
		super();
		this.logEntryMapper = logEntryMapper;
	}

	public Boolean validate() {

		try {
			logEntryMapper.list();
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
}
