package net.eldiosantos.logging.appender.appender.database.mybatis;

import java.util.List;

import net.eldiosantos.logging.appender.LogEntry;


public interface LogEntryMapper {
	LogEntry get(Long id);
	void save(LogEntry log);
	List<LogEntry>list();
	void createTable();
	void createSequence();
}
