package net.eldiosantos.logging.appender.appender.database.mybatis;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SqlSessionFactoryCreator {

	private static SqlSessionFactory sessionFactory;

	public SqlSessionFactory getFactory() {
		if(sessionFactory == null) {
			createSqlSessionFactory();
		}

		return sessionFactory;
	}

	private void createSqlSessionFactory() {
		Reader reader = null;

		try {
			InputStream in = this.getClass().getClassLoader().getResourceAsStream("myBatis-config.xml");
			reader = new InputStreamReader(in);

			if (sessionFactory == null) {
				sessionFactory = new SqlSessionFactoryBuilder().build(reader);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
