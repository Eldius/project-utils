package net.eldiosantos.utils.persistence;

import java.io.Serializable;
import java.util.List;

public interface Read<T, K extends Serializable>
{
    public T getByPk(K pk);
    public List<T>listAll();
    public List<T>list(int offset, int size);
	public List<T> getByExample(T element);
	public List<T> getByExamplePaginated(T element, int startIndex, int maxResults);
}