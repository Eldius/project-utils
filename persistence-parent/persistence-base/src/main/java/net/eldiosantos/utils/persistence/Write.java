package net.eldiosantos.utils.persistence;

import java.io.Serializable;

public interface Write<T, K extends Serializable>
{
    public K save(T element);
    public void update(T element);
    public void saveOrUpdate(T element);
}