package net.eldiosantos.utils.persistence.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import net.eldiosantos.utils.persistence.Repository;

import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;

public class BaseRepository<T, K extends Serializable> implements
		Repository<T, K> {
	protected Session session;
	protected Class<?> clazz;

	public BaseRepository(Session session) {
		this.session = session;
		this.clazz = (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public void delete(T element) {
		session.delete(element);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getByPk(K pk) {
		return (T) session.get(clazz, pk);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getByExample(T element) {
		return (List<T>) session
				.createCriteria(clazz)
				.add(
					Example.create(element)
					.enableLike(MatchMode.ANYWHERE)
					.ignoreCase()
				).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> listAll() {
		return session.createCriteria(clazz).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list(int offset, int size) {
		return session.createCriteria(clazz)
				.setFirstResult(offset)
				.setMaxResults(size)
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public K save(T element) {
		return (K) session.save(element);
	}

	@Override
	public void update(T element) {
		session.update(element);
	}

	@Override
	public void saveOrUpdate(T element) {
		session.saveOrUpdate(element);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getByExamplePaginated(T element, int startIndex,
			int maxResults) {
		return (List<T>) session
				.createCriteria(clazz)
				.add(
					Example.create(element)
					.enableLike(MatchMode.ANYWHERE)
					.ignoreCase()
				).setFirstResult(startIndex)
				.setMaxResults(maxResults)
				.list();
	}
}