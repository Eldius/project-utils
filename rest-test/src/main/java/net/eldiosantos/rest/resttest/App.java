package net.eldiosantos.rest.resttest;

import java.io.InputStream;
import java.util.Scanner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws ScriptException
    {
    	new App().test();
    }

    public void test() throws ScriptException {
    	
    	String response = "{\"id\":10012,\"cnpj\":\"33020512000179\",\"nome\":\"OSCAR ISKIN \",\"notasEmpenho\":\"2010NE901044;\",\"notas\":\"051219:051219:1;051192:051192:1;\",\"processInstance\":71004,\"documento\":\"6263678\",\"documentoProcesso\":\"6263679\",\"descricaoRetificacao\":null,\"emailFornecedor\":null,\"emailUsuario\":null,\"nomeUsuario\":null}";

    	String configFile = "config.json";
    	String config = getFileContent(configFile);
    	String scriptFile = "script.js";
    	String script = getFileContent(scriptFile);
		// create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create a JavaScript engine
        ScriptEngine engine = factory.getEngineByName("JavaScript");

//        Object configObject = engine.eval(config.toString());

        // evaluate JavaScript code from String
//        InputStream in = this.getClass().getClassLoader().getResourceAsStream(configFile);
		String executeScript = "var response = " + response + ";\n var config = " + config + ";" + script;
		System.out.println(executeScript);
		Object object = engine.eval(executeScript);
		
		System.out.println(object);

    }

	public String getFileContent(String configFile) {
		StringBuffer config = new StringBuffer();
		InputStream source = this.getClass().getClassLoader().getResourceAsStream(configFile);
    	final Scanner sc = new Scanner(source);
    	while(sc.hasNext()) {
    		config.append(sc.nextLine()).append("\n");
    	}
    	sc.close();
		return config.toString();
	}
}
