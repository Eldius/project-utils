package net.eldiosantos.rest.resttest;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigUrlParser {

	public String parseUrl(final String url, Map<String, Object>parameters) {

		final Pattern p = Pattern.compile("\\{(.+?)\\}");
		final Matcher m = p.matcher(url);

		final StringBuffer sb = new StringBuffer();

		int index = 0;
		while(m.find()){
	        final String key = m.group(1);
	        final String replacement = parameters.get(key).toString();
	        if(replacement != null){
	        	m.appendReplacement(sb, replacement);
	        }else{
	        	m.appendReplacement(sb, "");
	        }
	        index = m.end();
		}
		sb.append(url.subSequence(index, url.length()));

		return sb.toString();

	}
}
