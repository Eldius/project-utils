package net.eldiosantos.rest.resttest;

import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.eldiosantos.rest.resttest.json.JsonParser;
import net.eldiosantos.rest.resttest.scriptengine.ConfigParser;
import net.eldiosantos.rest.resttest.scriptengine.ContentFileLoader;
import net.eldiosantos.rest.resttest.scriptengine.EngineFactory;
import net.eldiosantos.rest.resttest.scriptengine.RestClient;
import net.eldiosantos.rest.resttest.scriptengine.ScriptRunner;

public class MainClass {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static void main(String[] args) throws ScriptException {

    	new MainClass().execute();
	}

	public void execute() throws ScriptException {
		EngineFactory factory = new EngineFactory();
		ScriptEngine engine = factory.getEngine("Groovy");
		JsonParser jsonParser = new JsonParser(engine);
		ContentFileLoader loader = new ContentFileLoader();

		ConfigParser parser = new ConfigParser(engine, jsonParser, loader);

		logger.debug("url: " + parser.getUrl());
		
		logger.debug("params:\n");
		Map<String, Object> parameters = parser.getParameters();
		for(String key:parameters.keySet()) {
			logger.debug("\t* " + key + ": " + parameters.get(key));
		}

		for(int i = 0; i < 50; i++) {
		String entity = new RestClient().makeRequest(parser, new ConfigUrlParser());

		final Bindings bindings = engine.createBindings();
		bindings.put("data", jsonParser.parse(loader, entity));
		logger.debug(new ScriptRunner(engine).execute(parser.getValidationScript(), bindings).toString());
		logger.debug(new ScriptRunner(engine).execute("data", bindings).toString());
		}
	}
}
