package net.eldiosantos.rest.resttest.json;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import net.eldiosantos.rest.resttest.scriptengine.ContentFileLoader;

public class JsonParser {

	private final ScriptEngine engine;

	public JsonParser(ScriptEngine engine) {
		super();
		this.engine = engine;
	}

	public Object parse(ContentFileLoader loader,
			String configStr) throws ScriptException {
		Bindings bindings = engine.createBindings();
		bindings.put("json", configStr);
		String scriptName = "GroovyParserScript.groovy";
		String script = loader.loadFileContent(scriptName);
		return engine.eval(script, bindings);
	}
}
