package net.eldiosantos.rest.resttest.scriptengine;

import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import net.eldiosantos.rest.resttest.json.JsonParser;

public class ConfigParser {

	private Map<String, Object>config;

	@SuppressWarnings("unchecked")
	public ConfigParser(ScriptEngine engine, JsonParser jsonParser, ContentFileLoader loader) {
		super();
		try {
			String fileName = "config.json";
			String configStr = loader.loadFileContent(fileName);
			this.config = (Map<String, Object>) jsonParser.parse(loader, configStr);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	public String getUrl() {
		return config.get("url").toString();
	}

	public Boolean isPost() {
		return config.get("method").toString().equalsIgnoreCase("post");
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getParameters() {
		return (Map<String, Object>) config.get("data");
	}

	public String getValidationScript() {
		return (String) config.get("validation");
	}
}
