package net.eldiosantos.rest.resttest.scriptengine;

import java.io.InputStream;
import java.util.Scanner;

public class ContentFileLoader {

	public String loadFileContent(final String fileName) {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
		Scanner sc = new Scanner(inputStream);
		StringBuffer content = new StringBuffer();
		while(sc.hasNext()) {
			content.append(sc.nextLine()).append("\n");
		}
		sc.close();

		return content.toString();
	}
}
