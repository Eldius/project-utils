package net.eldiosantos.rest.resttest.scriptengine;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

@SuppressWarnings("restriction")
public class EngineFactory {

	private static final ScriptEngineManager factory = new ScriptEngineManager();

	public ScriptEngine getEngine(final String engineName) {
		return factory.getEngineByName(engineName);
	}
}
