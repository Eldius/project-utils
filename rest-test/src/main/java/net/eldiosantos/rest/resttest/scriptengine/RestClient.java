package net.eldiosantos.rest.resttest.scriptengine;

import java.util.Map;

import net.eldiosantos.rest.resttest.ConfigUrlParser;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.representation.Form;

public class RestClient {

	public String makeRequest(ConfigParser configParser, ConfigUrlParser configUrlParser) {

		ClientConfig config;

		config = new DefaultClientConfig();
		config.getClasses().add(JacksonJsonProvider.class);
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

		Client client = Client.create(config);

		String url = configUrlParser.parseUrl(configParser.getUrl(), configParser.getParameters());
		WebResource resource = client.resource(url);

		Form form = new Form();

		Map<String, Object> parameters = configParser.getParameters();
		for(String key:parameters.keySet()) {
			form.add(key, parameters.get(key));
		}

		String entity = null;
		if(configParser.isPost()) {
			entity = resource.post(String.class, form);
		} else {
			entity = resource.get(String.class);
		}

		return entity;
	}
}
