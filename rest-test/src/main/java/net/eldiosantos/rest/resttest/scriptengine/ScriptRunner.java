package net.eldiosantos.rest.resttest.scriptengine;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class ScriptRunner {

	private ScriptEngine engine;

	public ScriptRunner(ScriptEngine engine) {
		super();
		this.engine = engine;
	}

	public Object execute(final String script, final Bindings bindings) throws ScriptException {
		return engine.eval(script, bindings);
	}
}
