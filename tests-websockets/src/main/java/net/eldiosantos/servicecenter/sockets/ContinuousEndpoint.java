package net.eldiosantos.servicecenter.sockets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint("/sockets/broadcast")
public class ContinuousEndpoint {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private static final List<String>phrases = Arrays.asList(
	    "O importante não é vencer todos os dias, mas lutar sempre."
	    , "Maior que a tristeza de não haver vencido é a vergonha de não ter lutado!"
	    , "O entusiasmo é a maior força da alma. Conserva-o e nunca te faltará poder para conseguires o que desejas."
	    , "Nossa maior fraqueza está em desistir. O caminho mais certo de vencer é tentar mais uma vez."
	    , "Quem quer vencer um obstáculo deve armar-se da força do leão e da prudência da serpente."
	    , "O único lugar onde o sucesso vem antes do trabalho é no dicionário."
	  );

    @OnMessage
    public String timer(String message) throws InterruptedException {
	logger.debug("message received: " + message);
	return "wake up!";
    }

    @OnOpen
    public void open(Session session) {
	logger.debug("Opening connection: " + session.getId());
	new Thread(new CustomProcess(session)).start();
    }

    @OnClose
    public void close(CloseReason reason) {
	logger.debug("Closing a WebSocket due to " + reason.getReasonPhrase());
    }

    private static class CustomProcess implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Session instance;

	public CustomProcess(Session instance) {
	    super();
	    this.instance = instance;
	}

	@Override
	public void run() {
	    while(instance.isOpen()) {
		try {
		    Thread.sleep(10000);
		    if(instance.isOpen()) {
			instance.getBasicRemote().sendText(getMessage());
		    }
		} catch (InterruptedException e) {
		    logger.error("Error while waiting", e);
		} catch (IOException e) {
		    logger.error("Error trying to send message", e);
		}
	    }
	}

	private String getMessage() {
	    String message = phrases.get(new Random().nextInt(phrases.size()));
	    return new SimpleDateFormat("[dd/MM/yyyy hh:mm:ss] ").format(new Date()) + message;
	}
    }
}
