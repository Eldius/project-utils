package net.eldiosantos.servicecenter.sockets;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint("/sockets/echo")
public class EchoPojo {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @OnMessage
    public String echo(String message) {
	logger.debug("Received : " + message);
	return message;
    }

    @OnOpen
    public void myOnOpen(Session session) {
	logger.debug("WebSocket opened: " + session.getId());
    }

    @OnClose
    public void myOnClose(CloseReason reason) {
	logger.debug("Closing a WebSocket due to " + reason.getReasonPhrase());
    }
}
