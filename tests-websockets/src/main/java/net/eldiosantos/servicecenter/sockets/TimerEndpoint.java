package net.eldiosantos.servicecenter.sockets;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint("/sockets/timer")
public class TimerEndpoint {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @OnMessage
    public String timer(final Long time) throws InterruptedException {
	logger.debug("Wait for: " + time);
	Thread.sleep(time);
	return "wake up!";
    }

    @OnOpen
    public void myOnOpen(Session session) {
	logger.debug("WebSocket opened: " + session.getId());
    }

    @OnClose
    public void myOnClose(CloseReason reason) {
	logger.debug("Closing a WebSocket due to " + reason.getReasonPhrase());
    }
}
