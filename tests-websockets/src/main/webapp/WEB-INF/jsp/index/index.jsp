
<h2>Home</h2>

<div style="text-align: center;">
	<form action="">
		<p>
			<input
				onclick="send_message()"
				value="Send"
				type="button">
			<input
				id="textID"
				name="message"
				value="Hello WebSocket!"
				type="text">
		</p>
	</form>
</div>
<div id="log">
<h3>Log:</h3>
</div>
<div id="output">
<h3>Test:</h3>
</div>

<script>
	function init() {
		$(".menu").find(".home").addClass("active");
		startTimer("#notifications");
	}

	function startTimer(selector) {
		var uri = getRootUri() + "/sockets/broadcast";

		getSocket(uri, selector);
	}

	var wsUri = getRootUri() + "/sockets/echo";

	function getRootUri() {
		return "ws://" + location.hostname + ":" + location.port + "${pageContext.request.contextPath}";
	}

	function init2() {
		output = document.getElementById("output");
	}

	function send_message() {

		websocket = new WebSocket(wsUri);
		websocket.onopen = function(evt) {
			onOpen(evt)
		};
		websocket.onmessage = function(evt) {
			onMessage(evt)
		};
		websocket.onerror = function(evt) {
			onError(evt)
		};

	}

	function onOpen(evt) {
		writeToScreen("Connected to Endpoint!");
		doSend(textID.value);

	}

	function onMessage(evt) {
		writeToScreen("Message Received: " + evt.data);
	}

	function onError(evt) {
		writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
	}

	function doSend(message) {
		writeToScreen("Message Sent: " + message);
		websocket.send(message);
	}

	function writeToScreen(message) {
		var pre = document.createElement("p");
		pre.style.wordWrap = "break-word";
		pre.innerHTML = message;

		output.appendChild(pre);
	}

	window.addEventListener("load", init2, false);
</script>
