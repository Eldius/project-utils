
	$(document).ready(function(){
		$(".deleteElement").click(function(){
			var url = $(this).data("delete-url");
			BootstrapDialog.show({
			    title: $(this).data("dialog-title"),
			    message: $(this).data("dialog-message"),
			    type: BootstrapDialog.TYPE_WARNING,
			    buttons: [{
	                label: $(this).data("dialog-yes-button"),
	                cssClass: 'btn-success',
	                action: function(dialogRef){
	                	window.location.replace(url);
	                }
	            }, {
	                label: $(this).data("dialog-no-button"),
	                cssClass: 'btn-warning',
	                action: function(dialogItself){
	                	dialogItself.close();
	                }
	            }]
			});
		});

		$("#clear_notifications").click(function(){
			var messages = $("#messageCount");
			var qtd = 0;
			messages.data("message-count", qtd)
			messages.html(qtd);

			$(".notify_message").remove();
		});
	});

	var getSocket = function(uri) {
		var websocket = new WebSocket(uri);

		var notifOk = "<li class='notify_message list-group-item list-group-item-success' role='alert'>-MESSAGE-</li>";
		var notifError = "<li class='notify_message list-group-item list-group-item-danger'>-MESSAGE-</li>";

		websocket.onopen = function(evt) {
			websocket.send(2000);
		};

		websocket.onmessage = function(evt) {
			var messages = $("#messageCount");
			var qtd = messages.data("message-count") + 1;
			messages.data("message-count", qtd)
			messages.html(qtd);
			$("#messages_divider").after(notifOk.replace("-MESSAGE-", evt.data));
		};

		websocket.onerror = function(evt) {
			var messages = $("#messageCount");
			var qtd = messages.data("message-count") + 1;
			messages.data("message-count", qtd)
			messages.html(qtd);
			$("#messages_divider").before(notifError.replace("-MESSAGE-", evt.data));
		};

		return websocket;	
	}

