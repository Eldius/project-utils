
		<!-- Notifications -->
		<div class="btn-group">
			<button
				class="btn btn-default btn-xs dropdown-toggle"
				type="button"
				data-toggle="dropdown">
				<fmt:message key="app.messages.display" />
				<span class="badge" id="messageCount" data-message-count="0">0</span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li>
					<a id="clear_notifications">
						<fmt:message key="app.messages.clear" />
					</a>
				</li>
				<li class="divider" id="messages_divider"></li>
			</ul>
		</div>
		<!-- Notifications -->
