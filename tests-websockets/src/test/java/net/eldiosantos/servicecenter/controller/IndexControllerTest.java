package net.eldiosantos.servicecenter.controller;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

public class IndexControllerTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testIndex() {
		assertEquals("Returning String", "You are ready to use VRaptor", new IndexController().index());
	}

}
